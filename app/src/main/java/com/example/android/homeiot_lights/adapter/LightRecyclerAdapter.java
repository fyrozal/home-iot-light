package com.example.android.homeiot_lights.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.homeiot_lights.DetailActivity;
import com.example.android.homeiot_lights.R;
import com.example.android.homeiot_lights.model.Light;
import com.example.android.homeiot_lights.model.LightList;
import com.example.android.homeiot_lights.util.CommonUtil;

public class LightRecyclerAdapter extends RecyclerView.Adapter<LightRecyclerAdapter.LightsViewHolder> {

    Context mContext;
    LayoutInflater mLayoutInflater;
    LightList mLightList;


    static LightItemClickListener nItemClickListener;

    public interface LightItemClickListener{
        void onItemClick(int position, View v);
    }

    public void setOnItemClickListener(LightItemClickListener clickListener){
        nItemClickListener = clickListener;
    }

    public LightRecyclerAdapter(Context context, LightList lightList) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mLightList = lightList;

    }

    @NonNull
    @Override
    public LightsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = mLayoutInflater.inflate(R.layout.item_card_view, viewGroup, false);
        return new LightsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LightsViewHolder lightsViewHolder, int position) {
        Light light = mLightList.getLights(position);
        lightsViewHolder.name.setText(light.getName());
//        light.getResourceIndex();

        Drawable drawable = CommonUtil.makeLightsType(mContext.getResources(), light.getResourceIndex());
        lightsViewHolder.imageView.setImageDrawable(drawable);


    }

    @Override
    public int getItemCount() {
        return mLightList.getTotalSize();
    }

    public static class LightsViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        TextView name;
        ImageView imageView;

        public LightsViewHolder(View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.txtName);
            this.imageView = itemView.findViewById(R.id.imageView3);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            nItemClickListener.onItemClick(getAdapterPosition(),v);

        }
    }


}
