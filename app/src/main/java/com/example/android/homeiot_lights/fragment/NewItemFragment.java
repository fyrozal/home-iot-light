package com.example.android.homeiot_lights.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.android.homeiot_lights.R;
import com.example.android.homeiot_lights.util.CommonUtil;

public class NewItemFragment extends Fragment {
    String[] lightNames;

    ImageView iView;
    EditText editText;
    Spinner spinner;
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.contain_new_lights, container, false);
         spinner = view.findViewById(R.id.spinner);
        iView = view.findViewById(R.id.imageView);
        editText = view.findViewById(R.id.editText);


        lightNames = getResources().getStringArray(R.array.lights_name);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, lightNames);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Drawable drawable = CommonUtil.makeLightsType(getResources(),position);
                iView.setImageDrawable(drawable);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return view;
    }

    public void setItemName(String name){
        this.editText.setText(name);
    }

    public void setItemResources(int index){
        this.spinner.setSelection(index);
    }

    public String getLightName() {
        return this.editText.getText().toString();
    }

    public int getResourceIndex() {
        return this.spinner.getSelectedItemPosition();
    }
}
