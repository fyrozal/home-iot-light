package com.example.android.homeiot_lights;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.homeiot_lights.adapter.LightRecyclerAdapter;
import com.example.android.homeiot_lights.fragment.NewItemFragment;
import com.example.android.homeiot_lights.model.Light;

public class DetailActivity extends AppCompatActivity  implements View.OnClickListener{
    TextView textView;

    int position = 0 ;
    Light light;

    Button btnUpdate;
    Button btnDelete;
    NewItemFragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        position = getIntent().getExtras().getInt(MainActivity.LIGHT_NUMBER);
        light = (Light) getIntent().getExtras().getSerializable(MainActivity.LIGHTS);

        btnUpdate = findViewById(R.id.button_update);
        btnDelete = findViewById(R.id.button_delete);

        btnUpdate.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

        fragment = (NewItemFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);


        fragment.setItemName(light.getName());
        fragment.setItemResources(light.getResourceIndex());


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_delete:
                sendDataToActivity(false);
                break;
            case R.id.button_update:
                sendDataToActivity(true);
                break;
        }

    }

    private void sendDataToActivity(boolean doSave) {
        Intent resultIntent = new Intent();
        Bundle bundle = new Bundle();

        if(doSave){
            String newName = fragment.getLightName();
            int newResourceIndex = fragment.getResourceIndex();

            Log.d("sendDataToActivity",""+newResourceIndex);
            Log.d("sendDataToActivity",newName);

            light.setName(newName);
            light.setResourceIndex(newResourceIndex);

            bundle.putSerializable(MainActivity.LIGHTS, light);

        }else{
            bundle.putSerializable(MainActivity.LIGHTS, null);

        }

        bundle.putInt(MainActivity.LIGHT_NUMBER, position);
        resultIntent.putExtras(bundle);
        setResult(RESULT_OK,resultIntent);
        this.finish();;
    }
}
