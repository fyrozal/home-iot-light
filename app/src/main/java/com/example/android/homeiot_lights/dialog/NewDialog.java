package com.example.android.homeiot_lights.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.homeiot_lights.MainActivity;
import com.example.android.homeiot_lights.R;
import com.example.android.homeiot_lights.model.Light;
import com.example.android.homeiot_lights.util.CommonUtil;

import org.w3c.dom.Text;

public class NewDialog extends DialogFragment {

    String[] lightNames;
    Spinner spinner;
    ImageView iView;
    int currentIndex;
    EditText editText;
    NoticeDialogListener listener;

    public interface NoticeDialogListener{
        public void onDialogPositiveClick(DialogFragment dialog,Light light);
        public void onDialogNegativeClick(DialogFragment dialog);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (NoticeDialogListener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.contain_new_lights, null);
        builder.setView(view);

        lightNames = getResources().getStringArray(R.array.lights_name);

        spinner = view.findViewById(R.id.spinner);
        iView = view.findViewById(R.id.imageView);
        editText = view.findViewById(R.id.editText);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, lightNames);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Drawable drawable = CommonUtil.makeLightsType(getResources(),position);
                iView.setImageDrawable(drawable);
                currentIndex = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setPositiveButton("save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = editText.getText().toString();
                if(TextUtils.isEmpty(name)){
                    name=lightNames[currentIndex];
                }
                Light light = new Light(name,currentIndex);
                listener.onDialogPositiveClick(NewDialog.this,light);

//                Toast.makeText(getActivity(), light.toString(), Toast.LENGTH_LONG).show();

            }

        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), lightNames[currentIndex], Toast.LENGTH_LONG).show();

            }
        });
//        builder.setTitle("New Dialog");
//        builder.setMessage("Hello Dialog");

        return builder.create();


    }

    public Drawable makeLightsType(int index) {
        TypedArray images = getResources().obtainTypedArray(R.array.light_resources);
        Drawable drawable = images.getDrawable(index);
        return drawable;
    }
}
