package com.example.android.homeiot_lights.model;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.android.homeiot_lights.service.LightService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LightList {

    private List<Light> lightList;

    private RecyclerView.Adapter recAdapter;
    private LightService lightService;

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://192.168.222.174:3000")
            .addConverterFactory(GsonConverterFactory.create())
            .build();


    public LightList() {
        lightList = new ArrayList<>();
        lightService = retrofit.create(LightService.class);

    }

    public void getLightList() {
        lightService.getLightsLiist().enqueue(new Callback<List<Light>>() {
            @Override
            public void onResponse(Call<List<Light>> call, Response<List<Light>> response) {
                Log.d("LightList", "getLightList : " + response.toString());
                if (response.isSuccessful()) {
                    lightList = response.body();
                    recAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Light>> call, Throwable t) {

            }
        });
//        return this.lightList;
    }


    public void addLight(Light newLight) {


        lightService.createLight(newLight).enqueue(new Callback<Light>() {
            @Override
            public void onResponse(Call<Light> call, Response<Light> response) {
                Log.d("LightList", "createLight : " + response.toString());
                if (response.isSuccessful()) {
                    Light newLight = response.body();
                    lightList.add(newLight);
                    recAdapter.notifyItemInserted(lightList.size() - 1);
                }
            }

            @Override
            public void onFailure(Call<Light> call, Throwable t) {

            }
        });

    }

    public Light getLights(int position) {
        return this.lightList.get(position);
    }

    public int getTotalSize() {
        return this.lightList.size();
    }

    int changeIndex = 0;

    public void removeLightAt(int position) {
        Light light = this.lightList.get(position);
        lightService.deleteLight(light).enqueue(new Callback<Integer>() {

            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                Log.d("LightList", "deleteLight : " + response.toString());
                if (response.isSuccessful()) {
                    int position = response.body();
                    lightList.remove(position);
                    recAdapter.notifyItemRemoved(position);

                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {

            }
        });

    }


    public void updateLights(int position, Light light) {
        changeIndex = position;
        lightService.updateLight(light).enqueue(new Callback<Light>() {
            @Override
            public void onResponse(Call<Light> call, Response<Light> response) {
                Log.d("LightList", "updateLights : " + response.toString());
                if (response.isSuccessful()) {
                    Light newLight = response.body();
                    lightList.set(changeIndex, newLight);

                    recAdapter.notifyItemChanged(changeIndex, newLight);
                }
            }

            @Override
            public void onFailure(Call<Light> call, Throwable t) {

            }
        });

//        this.lightList.set(position, light);
//        recAdapter.notifyItemChanged(position, light);
    }

    public void registerAdapter(RecyclerView.Adapter recAdapter) {
        this.recAdapter = recAdapter;
    }
}
