package com.example.android.homeiot_lights.model;

import java.io.Serializable;

public class Light implements Serializable {
    public static int ID=0;

    private int id;


    private int resourceIndex;
    private String name;

    public Light(String name, int resourceIndex){
        this.id = ID+1;
        this.name = name;
        this.resourceIndex = resourceIndex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResourceIndex() {
        return resourceIndex;
    }

    public void setResourceIndex(int resourceIndex) {
        this.resourceIndex = resourceIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "id : "+this.id+" name : "+this.name+" resourceIndex : "+this.resourceIndex;
    }

}
