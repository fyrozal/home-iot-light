package com.example.android.homeiot_lights;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.example.android.homeiot_lights.adapter.LightRecyclerAdapter;
import com.example.android.homeiot_lights.dialog.NewDialog;
import com.example.android.homeiot_lights.model.Light;
import com.example.android.homeiot_lights.model.LightList;

public class MainActivity extends AppCompatActivity implements NewDialog.NoticeDialogListener {
    public final static String MY_KEY = "NEW";
    public final static String LIGHTS = "LIGHTS";
    public final static String LIGHT_NUMBER = "LIGHT_NUMBER";
    public final static int MY_REQEUEST_CODE = 100;


    FloatingActionButton fab;
    LightList lightList;
    RecyclerView recyclerView;
    LightRecyclerAdapter lightRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lightList = new LightList();

        fab = findViewById(R.id.floatingActionButton);

        recyclerView = findViewById(R.id.light_list);


//        for (int i = 0; i < 10; i++) {
//            if(i%2 == 0 ){
//                lightList.addLight(new Light("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of " +
//                        "Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", 1));
//            }else{
//                lightList.addLight(new Light("Akbar.", 2));
//            }
//        }

        lightRecyclerAdapter = new LightRecyclerAdapter(this, lightList);
        lightRecyclerAdapter.setOnItemClickListener(new LightRecyclerAdapter.LightItemClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(LIGHT_NUMBER, position);
                bundle.putSerializable(LIGHTS, lightList.getLights(position));

                intent.putExtras(bundle);

                startActivityForResult(intent, MY_REQEUEST_CODE);

//                Toast.makeText(MainActivity.this, "Hello position : " + position, Toast.LENGTH_SHORT).show();
            }
        });
        lightList.registerAdapter(lightRecyclerAdapter);
        recyclerView.setAdapter(lightRecyclerAdapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewDialog nDialog = new NewDialog();
                nDialog.show(getSupportFragmentManager(), "Hello");

//                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                builder.setTitle("New Dialog");
//                builder.setMessage("Hello Dialog");
//                builder.create().show();

////                Toast.makeText(getApplicationContext(), "Hello Toast", Toast.LENGTH_SHORT).show();
//
//                Snackbar snackbar = Snackbar.make(findViewById(R.id.main), "HelloSnackbar", Snackbar.LENGTH_SHORT);
//                snackbar.setAction("Bye", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Toast.makeText(getApplicationContext(), "Hello Toast", Toast.LENGTH_SHORT).show();
//
//                    }
//                });
//                snackbar.show();

            }
        });

        lightList.getLightList();

    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog, Light light) {

        lightList.addLight(light);
//        lightRecyclerAdapter.notifyItemInserted(lightList.getTotalSize()-1);
        Toast.makeText(getApplicationContext(), "Hello Fragment", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainActivity.MY_REQEUEST_CODE) {
            if (resultCode == RESULT_OK) {
                int position = data.getIntExtra("LIGHT_NUMBER", 0);
                Light light = (Light) data.getSerializableExtra("LIGHT");
                if (light == null) {
                    lightList.removeLightAt(position);
                } else {
                    lightList.updateLights(position, light);
                }
            }
        }
    }


}
