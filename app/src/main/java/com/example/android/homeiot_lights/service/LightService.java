package com.example.android.homeiot_lights.service;

import com.example.android.homeiot_lights.model.Light;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface LightService  {

    @GET("/lights/list")
    Call<List<Light>> getLightsLiist();

    @POST("/lights/new")
    Call<Light> createLight(@Body Light light);

    @POST("/lights/update")
    Call<Light> updateLight(@Body Light light);


    @POST("/lights/delete")
    Call<Integer> deleteLight(@Body Light light);
}
